### How do I get set up? ###


general
```
#!bash

npm install -g sails
npm install -g ember-cli
```

in client...
```
#!bash

npm install && bower install
ember s --proxy http://localhost:1337
```

in server
```
#!bash

sails lift
```

Currently uses mongo--can easily be switched.

### Left off at ###
Not getting the game with associated cards loading properly--attempting trying a custom adapter/serializer pair, but that required removing the custom blueprints, which broke something else. Quite a hassle, really, haha.

Thanks for reading,
Max