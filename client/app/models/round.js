import DS from 'ember-data';

export default DS.Model.extend({
  timeSpent: DS.attr('number'),
  guessesMade: DS.attr('number'),
  isComplete: DS.attr('boolean'),
  game: DS.belongsTo('game'),

  totalCards: function(){
    return this.get('game.cardsLength');
  },

  score: function(){
    if(!this.get('isComplete')) { return 0; }

    var guessMultiplier = 0;
    var timeMultiplier = 0;
    var guesses = this.get('guessesMade');
    var guesses = this.get('timeSpent');

    if(guesses === 1){
      guessMultiplier = 1;
    }
    else if(guesses === 2){
      guessMultiplier = 0.75;
    } 
    else if(guesses === 3){
      guessMultiplier = 0.5;
    }

    if(timeSpent <= 10){
      timeMultiplier = 1;
    }
    else if(timeSpent <= 20){
      timeMultiplier = 0.9;
    } 
    else if(timeSpent <= 30){
      timeMultiplier = 0.8;
    }
    else {
      timeMultiplier = 0.75;
    }

    return guessMultiplier * timeMultiplier;
  }.property('guessesMade', 'timeSpent')
});