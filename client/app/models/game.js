import DS from 'ember-data';

export default DS.Model.extend({
  initials: DS.attr('string'),
  score: DS.attr('number'),
  cards: DS.hasMany('card', {async: true}),
  rounds: DS.hasMany('round', {async: true})
  
  cardsLength: function(){
    return this.get('cards').length;
  }
});