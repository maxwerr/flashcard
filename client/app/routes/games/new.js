import Ember from 'ember';

export default Ember.Route.extend({
  model: function(){

    var game = this.store.createRecord('game', {initials: "UNK", score: 0});
    var _this = this;

    this.store.find('card').then(function(res){
      game.get('cards').pushObjects(res.content);
      game.save().then(function(sgame){
        _this.transitionTo('game', sgame);
      });
    });
  }
});