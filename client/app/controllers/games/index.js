import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    clearAll: function(){
      return this.store.find('game').then(function(res){
        res.content.forEach(function(game){
          Ember.run.once(this, function(){
            game.deleteRecord();
            game.save();
          });
        }, this);
      });
    }
  }
});