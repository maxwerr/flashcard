import Ember from 'ember';

export default Ember.ObjectController.extend({
  actions: {
    delete: function(){
      var card = this.get('content');
      this.store.deleteRecord(card);
      card.save();
    },
    save: function(){
      var card = this.get('content');
      card.save();
      card.set("isEditing", false);
    },
    edit: function(){
      var card = this.get('content');
      card.set('isEditing', true);
    }
  }
});