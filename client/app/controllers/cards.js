import Ember from 'ember';

export default Ember.ArrayController.extend({
  itemController: 'card',
  actions: {
    create: function(){
      var card = this.store.createRecord('card');
      card.set('isEditing', true);
    }
  }
});