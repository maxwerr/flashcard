import Ember from 'ember';

var Router = Ember.Router.extend({
  location: ClientENV.locationType
});

Router.map(function() {
  this.resource('cards');
  this.resource('games', function(){
    this.resource('game', {path: '/:id'}, function(){
      this.resource('round', {path: '/round/:card_no'});
    });
    this.route('new');
  });
});

export default Router;